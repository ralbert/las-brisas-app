import { SET_TASKS, END_TASK } from './actionTypes';
import { uiStartLoading, uiStopLoading, authGetToken } from './index';
import axios from "axios";
import jwtDecode from "jwt-decode";
import { AsyncStorage } from 'react-native'

//--------------------------------------
//Function to export. It has the request for updating Tasks
//--------------------------------------
export const updateTask = (id) => {    
    return dispatch => {
        AsyncStorage.getItem('ap:localDB:planId', (err, planId) => { 
            AsyncStorage.getItem('ap:localDB:lavId', (err, lavId) => {
                dispatch(uiStartLoading());
                dispatch(authGetToken())
                .then(token => {                                                           
                    let tokenDecoded = jwtDecode(token);
                    let filteredName = tokenDecoded.userGroup.name.substr(0,3);
                    if (filteredName === "Cos"){
                        const headers = {
                            'Authorization': 'Bearer ' +  token
                        }
                        const data = {
                            'groupId': lavId,
                            'status': 'pendiente'
                        }
                        
                        return axios.patch("Hide... You don't have permission to see this code" + id, data, {  
                            headers: headers
                        });
                        
                    }  else if (filteredName === "Lav"){
                        const headers = {
                            'Authorization': 'Bearer ' +  token
                        }
                        const data = {
                            'groupId': planId,
                            'status': 'pendiente'
                        }
                        
                        return axios.patch("Hide... You don't have permission to see this code" + id, data, {  
                            headers: headers
                        });
                    } else {
                        const headers = {
                            'Authorization': 'Bearer ' +  token
                        }
                        const data = {
                            'status': 'completado'
                        }
                        return axios.patch("Hide... You don't have permission to see this code" + id, data, {  
                            headers: headers
                        });
                    }                  
                })
                .then((response) => {
                    console.log(response.data);
                    dispatch(uiStopLoading());
                })
                .catch(err => {
                    console.log(err);
                    alert("Something went wrong, please try again!" + err);
                    dispatch(uiStopLoading());
                });
            });
        });
    };
};

//--------------------------------------
//It configures the state
//--------------------------------------
export const endTask = (_id) => {
    return {
        type: END_TASK,
        taskId: _id
    };
};

//--------------------------------------
//Function to export. It has the request for getting Tasks
//--------------------------------------
export const getTasks = () => {
    return dispatch => {
        dispatch(authGetToken())
          .then(token => {
            let tokenDecoded = jwtDecode(token);
            let groupId = tokenDecoded.userGroup._id;
            return fetch("Hide... You don't have permission to see this code" + groupId, {
                method: 'GET',
                headers: {
                    "Authorization": "Bearer " + token
                }
            });
          })
          .catch(() => {
            alert("No valid token found!");
          })
          .then(res => {
            if (res.ok) {
              return res.json();
            } else {
              throw new Error();
            }
          })
          .then(parsedRes => {
            let arrayForIteration = [];
            arrayForIteration = parsedRes.tasks;
            const tasks = [];
            for (let _id in arrayForIteration) {
                tasks.push({
                    ...arrayForIteration[_id],
                    _id: arrayForIteration[_id]._id, 
                    code: arrayForIteration[_id].code,
                    title: arrayForIteration[_id].title,
                    detail: arrayForIteration[_id].detail,
                    pubdate: arrayForIteration[_id].pubdate,
                    importance: arrayForIteration[_id].importance,
                    image: {
                      uri: "Hide... You don't have permission to see this code"
                    }
                });
            }
            dispatch(setTasks(tasks));
          })
          .catch(err => {
            alert("Something went wrong, sorry :/");
            console.log(err);
          });
      };
};

//--------------------------------------
//It configures the state
//--------------------------------------
export const setTasks = tasks => {
    return {
        type: SET_TASKS,
        tasks: tasks
    };
};