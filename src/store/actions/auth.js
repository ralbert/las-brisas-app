import { AsyncStorage } from "react-native";

import { AUTH_SET_TOKEN, AUTH_REMOVE_TOKEN } from "./actionTypes";
import { uiStartLoading, uiStopLoading } from "./index";
import startMainTabs from "../../screens/MainTabs/startMainTabs";
import App from "../../../App";
import { Alert } from 'react-native'
import jwtDecode from "jwt-decode";

//--------------------------------------
//It exports the different methods for authorization purposes
//--------------------------------------

//--------------------------------------
//It allows the user to login
//--------------------------------------
export const tryAuth = (authData) => {
  return dispatch => {
    dispatch(uiStartLoading());
    let url ="Hide... You don't have permission to see this code"
    fetch(url, {
      method: "POST",
      body: JSON.stringify({
        email: authData.email,
        password: authData.password
      }),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .catch(err => {
        console.log(err);
        alert("Los datos insertados son incorrectos. Por favor itentelo nuevamente");
        dispatch(uiStopLoading());
      })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          throw new Error();
        }
      })
      .then(parsedRes => {
        dispatch(uiStopLoading());
        console.log(parsedRes);
        if (!parsedRes.token) {
            Alert.alert(
                'Autenticación Fallida',
                'Los datos insertados son incorrectos. Por favor itentelo nuevamente!'
            )
        } else {                
          dispatch(
            authStoreToken(
              parsedRes.token,
              parsedRes.expiresIn,
              parsedRes.refreshToken
            )
          );
          dispatch(authGetToken())
          .then(token => {
            let tokenDecoded = jwtDecode(token);
            let userRole = tokenDecoded.userRole.name;
            AsyncStorage.setItem("ap:localDB:userRole", userRole)
            return fetch("Hide... You don't have permission to see this code", {
                method: 'GET',
                headers: {
                    "Authorization": "Bearer " + token
                }
            });
          })
          .catch(() => {
            alert("No valid token found!");
          })
          .then(res => {
            if (res.ok) {
              return res.json();
            } else {
              throw new Error();
            }
          })
          .then(parsedRes => {
            let planId = "";
            let lavId = "";
            let arrayForIteration = [];
            arrayForIteration = parsedRes.groups;
            for (let _id in arrayForIteration) {
              if (arrayForIteration[_id].name === "Plan01"){
                planId = arrayForIteration[_id]._id
              } else if (arrayForIteration[_id].name === "Lav01") {
                lavId = arrayForIteration[_id]._id
              }

            }

            AsyncStorage.setItem("ap:localDB:planId", planId)
            AsyncStorage.setItem("ap:localDB:lavId", lavId)
            startMainTabs();  
          })
          .catch(err => {
            alert("Algo está mal :/" + err);
            console.log(err);
          });      
        }
      });
  };
};

//--------------------------------------
//Stores the token in "local storage"
//--------------------------------------
export const authStoreToken = (token, expiresIn, refreshToken) => {
  return dispatch => {
    const now = new Date();
    const expiryDate = now.getTime() + expiresIn * 1000;
    dispatch(authSetToken(token, expiryDate));
    AsyncStorage.setItem("ap:auth:token", token);
    AsyncStorage.setItem("ap:auth:expiryDate", expiryDate.toString());
    AsyncStorage.setItem("ap:auth:refreshToken", refreshToken);
  };
};

//--------------------------------------
//It configures the state
//--------------------------------------
export const authSetToken = (token, expiryDate) => {
  return {
    type: AUTH_SET_TOKEN,
    token: token,
    expiryDate: expiryDate
  };
};

//--------------------------------------
//This Function ensures that the app gets the token, if not it looks for it in "Local Storage"
//--------------------------------------
export const authGetToken = () => {
  return (dispatch, getState) => {
    const promise = new Promise((resolve, reject) => {
        const token = getState().auth.token;
        const expiryDate = getState().auth.expiryDate;
        if (!token|| new Date(expiryDate) <= new Date()) {
          let fetchedToken;
          AsyncStorage.getItem("ap:auth:token")//promesa
          .catch(err => reject())
          .then(tokenFromStorage => {
            fetchedToken = tokenFromStorage;
            if (!tokenFromStorage) {
              reject();
              return;
            }
            return AsyncStorage.getItem("ap:auth:expiryDate");
          })
          .then(expiryDate => {
            const parsedExpiryDate = new Date(parseInt(expiryDate));
            const now = new Date();
            if (parsedExpiryDate > now) {
              dispatch(authSetToken(fetchedToken));
              resolve(fetchedToken);
            } else {
              reject();
            }
          })
          .catch(err => reject());
        } else {
            resolve(token);
        }
    });  
    return promise
      .catch(err => {
        return AsyncStorage.getItem("ap:auth:refreshToken")
          .then(refreshToken => {
            return fetch(
              "Hide... You don't have permission to see this code",
              {
                method: "POST",
                headers: {
                  "Content-Type": "application/json"
                },
                body: {
                  email: "01@ejemplo.com",
                  refreshToken: refreshToken
                }
              }
            );
          })
          .then(res => {
            if (res.ok) {
              return res.json();
            } else {
              throw new Error();
            }
          })
          .then(parsedRes => {
            if (parsedRes.token) {
              console.log("Refresh token worked!");
              dispatch(
                authStoreToken(
                  parsedRes.token,
                  parsedRes.expires_in,
                  parsedRes.refresh_token
                )
              );
              return parsedRes.token;
            } else {
              dispatch(authClearStorage());
            }
          });
      })
      .then(token => {
        if (!token) {
          throw new Error();
        } else {
          return token;
        }
      });
  };
};

//--------------------------------------
//Starts the main menu
//--------------------------------------
export const authAutoSignIn = () => {
  return dispatch => {
    dispatch(authGetToken())
      .then(token => {
        startMainTabs();
      })
      .catch(err => console.log("Failed to fetch token!"));
  };
};

//--------------------------------------
//Clean up the "Local Storage"
//--------------------------------------
export const authClearStorage = () => {
  return dispatch => {
    AsyncStorage.removeItem("ap:auth:token");
    AsyncStorage.removeItem("ap:auth:expiryDate");
    AsyncStorage.removeItem("ap:localDB:planId");
    AsyncStorage.removeItem("ap:localDB:lavId");
    AsyncStorage.removeItem("ap:localDB:userRole");
    return AsyncStorage.removeItem("ap:auth:refreshToken");
  };
};

//--------------------------------------
//Manage the logout
//--------------------------------------
export const authLogout = () => {
  return dispatch => {
    dispatch(authClearStorage()).then(() => {
      App();
    });
    dispatch(authRemoveToken());
  };
};

//--------------------------------------
//It configures the state
//--------------------------------------
export const authRemoveToken = () => {
  return {
    type: AUTH_REMOVE_TOKEN
  };
};