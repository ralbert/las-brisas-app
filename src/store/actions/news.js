import { SET_NEWS } from './actionTypes';
import { authGetToken } from './index';

//--------------------------------------
//Function to export. It has the request for getting News from the API
//--------------------------------------
export const getNews = () => {
    return dispatch => {
        dispatch(authGetToken())
          .then(token => {
            return fetch("Hide... You don't have permission to see this code", {
                method: 'GET',
                headers: {
                    "Authorization": "Bearer " + token
                }
            });
        })
        .catch(err => {
            alert("Lo sentimos. Algo salió mal con tu petición." +
            "Vuelve a intentarlo en un rato");
            console.log(err);
        })
        .then(res => {
            if (res.ok) {
              return res.json();
            } else {
              throw new Error();
            }
          })
        .then(parsedRes => {            
            let arrayForIteration = [];
            arrayForIteration = parsedRes.news;
            const news = [];
            for (let _id in arrayForIteration) {
                news.push({
                    ...arrayForIteration[_id],
                    _id: arrayForIteration[_id]._id, 
                    title: arrayForIteration[_id].title,
                    detail: arrayForIteration[_id].detail,
                    image: {
                        uri: "Hide... You don't have permission to see this code"
                    }
                });
            }
            dispatch(setNews(news));
        });
    };
};

//--------------------------------------
//It configures the state
//--------------------------------------
export const setNews = news => {
    return {
        type: SET_NEWS,
        news: news
    };
};