import { END_TASK, SET_TASKS } from "../actions/actionTypes";

const initialState = {
  tasks: [
    //Just for testing purposes
    /*
    {
      _id: "01", 
      code: "cos01",
      title: "Cosecha Corte 24",
      detail: "Recolectar 8 bines de piña num 5 verde del corte 24."+
      "En caso de ser necesario pasar a corte 25 y avisar a encargado",
      pubdate: "24-07-2019",
      importance: "Maxima",
      status: "Pendiente",
      image: {
        uri: "http://www.lacalle.cu/wp-content/uploads/2016/07/campesino-pi%C3%B1al-1-600x358.jpg"
      }
    },
    {
      _id: "02", 
      code: "cos02",
      title: "Cosecha Corte 45, 46 y 47",
      detail: "Recolectar 20 bines de piña num 6 pintona de los cortes 45,46 y 47." +
      "En caso de no salir con las cantidades solicitar como proceder a Luis Vargas",
      pubdate: "24-07-2019",
      importance: "Media",
      status: "Pendiente",
      image: {
        uri: "http://www.lacalle.cu/wp-content/uploads/2016/07/campesino-pi%C3%B1al-1-600x358.jpg"
      }
    },
    {
      _id: "03", 
      code: "cos03",
      title: "Cosecha Corte 201",
      detail: "Recolectar 2 bines de piña num 3 madura. La misma es para jalea. Si hay madura de otros tamaños" +
      "también se puede cosechar. En caso de que sobre piña madura favor avisar a Rodrigo Cordero",
      pubdate: "24-07-2019",
      importance: "Baja",
      status: "Pendiente",
      image: {
        uri: "http://www.lacalle.cu/wp-content/uploads/2016/07/campesino-pi%C3%B1al-1-600x358.jpg"
      }
    }*/
  ]
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_TASKS:
    return {
      ...state,
      tasks: action.tasks
    };
    case END_TASK:
      return {
        ...state,
        tasks: state.tasks.filter(task => {
          return task._id !== action.taskId;
        })
      };
    default:
      return state;
  }
};

export default reducer;