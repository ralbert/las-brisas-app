import React, { Component } from "react";
import {
  StyleSheet,
  ImageBackground,
  Dimensions,
  Text
} from "react-native";
import backgroundImage from "../../assets/lasBrisasLoginBackground.jpg";

//--------------------------------------
//This Screen is not visible. It is just for testing purposes.
//--------------------------------------
class MainMenuScreen extends Component {
  static navigatorStyle = {
    navBarButtonColor: "orange"
  }
  state = {
    
  };

  constructor(props) {
    super(props);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
  }
  onNavigatorEvent = event => {
    if (event.type === "NavBarButtonPress") {
      if (event.id === "sideDrawerToggle") {
        this.props.navigator.toggleDrawer({
          side: "left"
        });
      }
    }
  };
  //previo al cierre del componente el borra todo
  componentWillUnmount() {
    Dimensions.removeEventListener("change", this.updateStyles);
  }

  render() {
    return (
      <ImageBackground source={backgroundImage} style={styles.backgroundImage}>
          <Text>Menú principal</Text>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  headingTextStyle: {
    color: "white",
  },
  backgroundImage: {
    width: "100%",
    flex: 1
  },
  inputContainer: {
    width: "80%"
  },
  input: {
    backgroundColor: "#eee",
    borderColor: "#bbb"
  },
  landscapePasswordContainer: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  portraitPasswordContainer: {
    flexDirection: "column",
    justifyContent: "flex-start"
  },
  landscapePasswordWrapper: {
    width: "100%"
  },
  portraitPasswordWrapper: {
    width: "100%"
  }
});


export default MainMenuScreen;