import React, { Component } from "react";
import {
  View,
  Image,
  Text,
  StyleSheet,
  TouchableOpacity,
  Platform,
  Dimensions,
  ScrollView,
  ActivityIndicator,
  Alert,
  AsyncStorage
} from "react-native";
import { connect } from "react-redux";

import Icon from "react-native-vector-icons/Ionicons";
import { endTask , updateTask } from "../../store/actions/index";

//--------------------------------------
//It shows Tasks details
//--------------------------------------
class TaskDetail extends Component {
    state = {
        viewMode: "portrait",
        taskId: ""
    };

    constructor(props) {
        super(props);
        Dimensions.addEventListener("change", this.updateStyles);
    }

    componentWillUnmount() {
        Dimensions.removeEventListener("change", this.updateStyles);
    }

    updateStyles = dims => {
        this.setState({
          viewMode: dims.window.height > 500 ? "portrait" : "landscape"
        });
    };

    taskUpdatedHandler = () => { 
      //It takes the role from the LocalStorage to know how to update the task element based on the role
      AsyncStorage.getItem('ap:localDB:userRole', (err, userRole) => { 
        if (userRole === "administrador"){
          Alert.alert(
            'Atención',
            'Está a punto de finalizar una tarea. Desea continuar?',
            [
              {
                text: 'Cancelar',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
              {text: 'Finalizar Tarea', onPress: () => {
                this.props.onUpdateTask(this.props.selectedTask._id);
                this.props.onEndTask(this.props.selectedTask._id);
                this.props.navigator.pop();
              }},
            ],
            {cancelable: false},
          );   
        }else {
          this.props.navigator.pop();
        }
      })
    };

    taskCloseHandler = () => { 
      this.props.navigator.pop();
    };

    render() {
        let submitButton = (         
            <TouchableOpacity onPress={this.taskUpdatedHandler}>
              <View style={styles.endButton}>
                <Icon
                  size={64} 
                  name={Platform.OS === "android" ? "md-checkbox" : "ios-checkbox"}
                  color="red"
                />
              </View>
            </TouchableOpacity>
        );
        if (this.props.isLoading) {
          submitButton = <ActivityIndicator />;
        }
        return (
          <View
            style={[
              styles.container,
              this.state.viewMode === "portrait"
                ? styles.portraitContainer
                : styles.landscapeContainer
            ]}
          >        
            <ScrollView style={styles.modalContainer}>
              <View>
                  <Image source={this.props.selectedTask.image} style={styles.taskImage} />
                  <Text style={styles.taskTitle}>{this.props.selectedTask.title}</Text> 
                  <View style={styles.taskCodeGroup }>
                    <Text style={styles.taskCodeLabel}>Código: </Text> 
                    <Text style={styles.taskCode}>{this.props.selectedTask.code}</Text> 
                  </View> 
                  <Text style={styles.taskCodeLabel}>Detalles: </Text>      
                  <Text style={styles.taskDetail}>{this.props.selectedTask.detail}</Text>
              </View>
              <View>
                {submitButton}
              </View>    
            </ScrollView> 
          </View>               
        );
    }
}

const styles = StyleSheet.create({
  modalContainer: {
    margin: 22
  },

  taskImage: {
    width: "100%",
    height: 200
  },

  taskTitle: {
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 28
  },

  taskCodeGroup: {
    flexDirection: "row",
    marginBottom: 20
  },
  taskCodeLabel: {
    fontWeight: "bold",
    fontSize: 24
  },
  taskCode: {
    fontSize: 24
  },


  taskDetailLabel: {
    fontWeight: "bold",
    fontSize: 24,
    width: "30%"
  },
  taskDetail: {
    fontSize: 24,
    marginBottom: 20
  },

  endButton: {
    alignItems: "center"
  },

});

const mapStateToProps = state => {
  return {
    isLoading: state.ui.isLoading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onUpdateTask: _id => dispatch(updateTask(_id)),
    onEndTask: _id => dispatch(endTask(_id))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TaskDetail);