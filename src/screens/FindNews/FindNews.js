import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Animated
} from "react-native";
import { connect } from "react-redux";
import NewsList from "../../components/NewsList/NewsList";
import { getNews } from "../../store/actions/index";

//--------------------------------------
//It shows News for the user
//--------------------------------------
class FindNewsScreen extends Component {
  static navigatorStyle = {
    navBarButtonColor: "orange"
  }

  state = {
    newsLoaded: false,
    removeAnim: new Animated.Value(1),
    newsAnim: new Animated.Value(0)
  };

  //--------------------------------------
  //This block makes visble the drawer in the view
  //--------------------------------------
  constructor(props) {
    super(props);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
  }

  componentDidMount() {
    this.props.onLoadNews();
  }

  onNavigatorEvent = event => {
    if (event.type === "NavBarButtonPress") {
      if (event.id === "sideDrawerToggle") {
        this.props.navigator.toggleDrawer({
          side: "left"
        });
      }
    }
  };
  //--------------------------------------

  //--------------------------------------
  //Animation
  //--------------------------------------
  newsLoadedHandler = () => {
    Animated.timing(this.state.newsAnim, {
      toValue: 1,
      duration: 500, 
      useNativeDriver: true 
    }).start();
  };

  newsSearchHandler = () => {
    Animated.timing(this.state.removeAnim, {
      toValue: 0,
      duration: 500,
      useNativeDriver: true
    }).start(() => {
      this.setState({
        newsLoaded: true
      });
      this.newsLoadedHandler();
    });
  };
//--------------------------------------

  itemSelectedHandler = key => {
    const selNewVal = this.props.news.find(newVal => {
      return newVal._id === key;
    });
    this.props.navigator.push({
      screen: "las-brisas.NewsDetailScreen",
      title: selNewVal.title,
      passProps: { 
        selectedNew: selNewVal
      }
    });
  };

  render() {
    let content = (
      <Animated.View
        style={{
          opacity: this.state.removeAnim,
          transform: [
            {
              scale: this.state.removeAnim.interpolate({
                inputRange: [0, 1],
                outputRange: [12, 1]
              })
            }
          ]
        }}
      >
        <TouchableOpacity onPress={this.newsSearchHandler}>
          <View style={styles.searchButton}>
            <Text style={styles.searchButtonText}>Buscar</Text>
          </View>
        </TouchableOpacity>
      </Animated.View>
    );
    if (this.state.newsLoaded) {
      content = (
        <Animated.View
          style={{
            opacity: this.state.newsAnim
          }}
        >
          <NewsList
            news={this.props.news}
            onItemSelected={this.itemSelectedHandler}
          />
        </Animated.View>
      );
    }
    return (
      <View style={this.state.newsLoaded ? null : styles.buttonContainer}>
        {content}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  buttonContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  searchButton: {
    borderColor: "blue",
    borderWidth: 3,
    borderRadius: 50,
    padding: 20
  },
  searchButtonText: {
    color: "blue",
    fontWeight: "bold",
    fontSize: 26
  }
});

const mapStateToProps = state => {
  return {
    news: state.news.news
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onLoadNews: () => dispatch(getNews())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FindNewsScreen);