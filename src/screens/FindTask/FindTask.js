import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Animated
} from "react-native";
import { connect } from "react-redux";
import TaskList from "../../components/TaskList/TaskList";
import { getTasks } from "../../store/actions/index";

//--------------------------------------
//It shows tasks for the user
//--------------------------------------
class FindTaskScreen extends Component {
  //Drawer
  static navigatorStyle = {
    navBarButtonColor: "orange"
  }

  state = {
    tasksLoaded: false,  //used only for animation 
    removeAnim: new Animated.Value(1),  //the opacity will be set to (1)
    tasksAnim: new Animated.Value(0) //It is not visible
  };

  constructor(props) {
    super(props);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
  }

  //onLoadTasks is defined at the end of this document: 
  //It gets access to the *getTasks()* declared in store/actions/tasks.js through the dispatch function 
  componentDidMount() {
    this.props.onLoadTasks();
  }

  //It allows the Side drawer to visible in this screen
  onNavigatorEvent = event => {
    if (event.type === "NavBarButtonPress") {
      if (event.id === "sideDrawerToggle") {
        this.props.navigator.toggleDrawer({
          side: "left"
        });
      }
    }
  };



  //--------------------------------------
  //Animation
  //--------------------------------------
  tasksLoadedHandler = () => {
    //timing gives some time before the animation happens
    Animated.timing(this.state.tasksAnim, {
      toValue: 1, //makes the animation visible
      duration: 500, 
      useNativeDriver: true //Improves the animation performance 
    }).start();
  };

  tasksSearchHandler = () => {
    //timing gives some time before the animation happens
    Animated.timing(this.state.removeAnim, {
      toValue: 0, //makes the animation invisible
      duration: 500,
      useNativeDriver: true
    }).start(() => {
      this.setState({
        tasksLoaded: true
      });
      this.tasksLoadedHandler();
    });
  };
  //--------------------------------------

  itemSelectedHandler = key => {
    const selTask = this.props.tasks.find(task => {
      return task._id === key;
    });
    //It opens the detail task screen
    this.props.navigator.push({
      screen: "las-brisas.TaskDetailScreen",
      title: selTask.title,
      passProps: { 
        selectedTask: selTask
      }
    });
  };

  render() {
    let content = (
      <Animated.View
        style={{
          opacity: this.state.removeAnim,
          transform: [
            { //interpolate: allows us to determine the element behavior 
              scale: this.state.removeAnim.interpolate({
                inputRange: [0, 1],
                outputRange: [12, 1]
              })
            }
          ]
        }}
      >
        <TouchableOpacity onPress={this.tasksSearchHandler}>
          <View style={styles.searchButton}>
            <Text style={styles.searchButtonText}>Buscar Tareas Pendientes</Text>
          </View>
        </TouchableOpacity>
      </Animated.View>
    );
    if (this.state.tasksLoaded) {
      content = (
        <Animated.View
          style={{
            opacity: this.state.tasksAnim
          }}
        >
          <TaskList
            tasks={this.props.tasks}
            onItemSelected={this.itemSelectedHandler}
          />
        </Animated.View>
      );
    }
    return (
      <View style={this.state.tasksLoaded ? null : styles.buttonContainer}>
        {content}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  buttonContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  searchButton: {
    borderColor: "orange",
    borderWidth: 3,
    borderRadius: 50,
    padding: 20
  },
  searchButtonText: {
    color: "orange",
    fontWeight: "bold",
    fontSize: 26
  }
});

const mapStateToProps = state => {
  return {
    tasks: state.tasks.tasks
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onLoadTasks: () => dispatch(getTasks())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FindTaskScreen);