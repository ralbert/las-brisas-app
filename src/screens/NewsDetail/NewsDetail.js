import React, { Component } from "react";
import {
  View,
  Image,
  Text,
  StyleSheet,
  TouchableOpacity,
  Platform,
  Dimensions,
  ScrollView,
  ActivityIndicator
} from "react-native";
import { connect } from "react-redux";

import Icon from "react-native-vector-icons/Ionicons";

//--------------------------------------
//It shows news details
//--------------------------------------
class NewsDetail extends Component {
    state = {
        viewMode: "portrait",
        newValId: ""
    };

    constructor(props) {
        super(props);
        Dimensions.addEventListener("change", this.updateStyles);
    }

    componentWillUnmount() {
        Dimensions.removeEventListener("change", this.updateStyles);
    }

    updateStyles = dims => {
        this.setState({
          viewMode: dims.window.height > 500 ? "portrait" : "landscape"
        });
    };

    newValCloseHandler = () => { 
      this.props.navigator.pop();
    };

    render() {
        let submitButton = (
          <TouchableOpacity onPress={this.newValCloseHandler}>
            <View style={styles.endButton}>
              <Icon
                size={64} 
                name={Platform.OS === "android" ? "md-close" : "ios-close"}
                color="red"
              />
            </View>
          </TouchableOpacity>
        );
        if (this.props.isLoading) {
          submitButton = <ActivityIndicator />;
        }
        return (
          <View
            style={[
              styles.container,
              this.state.viewMode === "portrait"
                ? styles.portraitContainer
                : styles.landscapeContainer
            ]}
          >        
            <ScrollView style={styles.modalContainer}>
              <View>
                  <Image source={this.props.selectedNew.image} style={styles.newValImage} />
                  <Text style={styles.newValTitle}>{this.props.selectedNew.title}</Text> 
                  <Text style={styles.newValDetailLabel}>Detalles: </Text>      
                  <Text style={styles.newValDetail}>{this.props.selectedNew.detail}</Text>
              </View>
              <View>
                {submitButton}
              </View>    
            </ScrollView> 
          </View>               
        );
    }
}

const styles = StyleSheet.create({
  modalContainer: {
    margin: 22
  },

  newValImage: {
    width: "100%",
    height: 200
  },

  newValTitle: {
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 28
  },

  newValDetailLabel: {
    fontWeight: "bold",
    fontSize: 24
  },
  newValDetail: {
    fontSize: 24,
    marginBottom: 20
  },

  endButton: {
    alignItems: "center"
  },

});

const mapStateToProps = state => {
  return {
    isLoading: state.ui.isLoading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onUpdateTask: _id => dispatch(updateTask(_id)),
    onEndTask: _id => dispatch(endTask(_id))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsDetail);