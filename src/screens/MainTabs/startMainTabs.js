import { Navigation } from 'react-native-navigation';
import { Platform } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

//--------------------------------------
//It shows up the following screens:
//FindTask
//FindNews
//Drawer
//--------------------------------------
const startTabs = () => {
    //Promise for getting icons
    Promise.all([
        Icon.getImageSource(Platform.OS === 'android' ? "md-hammer" : "ios-hammer", 30),
        Icon.getImageSource(Platform.OS === 'android' ? "md-search" : "ios-search", 30),
        Icon.getImageSource(Platform.OS === 'android' ? "md-home" : "ios-home", 30),
        Icon.getImageSource(Platform.OS === 'android' ? "md-menu" : "ios-menu", 30)
    ]).then(sources => { 
        Navigation.startTabBasedApp({
            tabs: [
                {
                    screen: "las-brisas.FindTaskScreen",
                    label: "Tus tareas",
                    title: "Tus tareas",
                    icon: sources[0], 
                    navigatorButtons: {
                        leftButtons: [
                            {
                                icon: sources[3],
                                title: "Menu",
                                id: "sideDrawerToggle"
                            }
                        ]
                    }
                },
                /*
                {
                    screen: "las-brisas.MainMenuScreen",
                    label: "Menú principal",
                    title: "Menú Principal",
                    icon: sources[2], //de la segunda promesa
                    navigatorButtons: { //permite desplegar  al sidedrawer
                        leftButtons: [
                            {
                                icon: sources[3],
                                title: "Menu",
                                id: "sideDrawerToggle"
                            }
                        ]
                    }
                },*/
                {
                    screen: "las-brisas.FindNewsScreen",
                    label: "Noticias",
                    title: "Noticias",
                    icon: sources[1],
                    navigatorButtons: {
                        leftButtons: [
                            {
                                icon: sources[3],
                                title: "Menu",
                                id: "sideDrawerToggle"
                            }
                        ]
                    }
                }
            ],
            tabsStyle: {
                tabBarSelectedButtonColor: "orange"
            },
            drawer: {
                left: {
                    screen: "las-brisas.SideDrawer"
                }
            },
            appStyle: {
                tabBarSelectedButtonColor: "orange"
            },
        });
    });
};

export default startTabs;
