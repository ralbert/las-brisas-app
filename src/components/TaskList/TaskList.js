import React from 'react';
import {StyleSheet, FlatList} from 'react-native';
import ListItem from "../ListItem/ListItem";

//--------------------------------------
//Component for listing Tasks elements
//--------------------------------------
const taskList = props => {
    return (
        <FlatList 
          style={styles.listContainer}
          data={props.tasks}
          renderItem={(info) => (
            <ListItem 
              title={info.item.title} 
              placeImg={info.item.image}
              onItemPressed={() => props.onItemSelected(info.item._id)}>
            </ListItem>
          )}
        />
    );
};

const styles = StyleSheet.create({
    listContainer: {
      width: "100%"
    }
  });

export default taskList;