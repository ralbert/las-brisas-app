import React from 'react';
import {StyleSheet, FlatList} from 'react-native';
import ListItem from "../ListItem/ListItem";

//--------------------------------------
//Component for listing News elements
//--------------------------------------
const taskList = props => {
    return (
        <FlatList 
          style={styles.listContainer}
          data={props.news}
          renderItem={(info) => (
            <ListItem 
              title={info.item.title} 
              placeImg={info.item.image}
              onItemPressed={() => props.onItemSelected(info.item._id)}>
            </ListItem>
          )}
        />
    );
};

const styles = StyleSheet.create({
    listContainer: {
      width: "100%"
    }
  });

export default taskList;