import React from "react";
import { Text, StyleSheet } from "react-native";

//--------------------------------------
//Componet for Titles 
//--------------------------------------
const headingText = props => (
  <Text {...props} style={[styles.textHeading, props.style]}>
    {props.children}
  </Text>
);

const styles = StyleSheet.create({
  textHeading: {
    color: "white",
    fontSize: 28,
    fontWeight: "bold"
  }
});

export default headingText;
