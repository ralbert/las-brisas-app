import React from 'react';
import { Text, StyleSheet } from 'react-native';

//--------------------------------------
//For text blocks 
//--------------------------------------
const mainText = props => (
    <Text style={styles.mainText}>{props.children}</Text>
);

const styles = StyleSheet.create({
    mainText: {
        color: "black"
    }
});

export default mainText;