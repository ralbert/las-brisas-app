import React from "react";
import { TextInput, StyleSheet } from "react-native";

//--------------------------------------
//Input Component
//--------------------------------------
const defaultInput = props => (
  <TextInput
    underlineColorAndroid="transparent"
    {...props} 
    //obtiene cualquier prop definido en el padre
    //style se define de la siguiente manera para 
    //concatenar o adjuntar los estilos del padre
    //con los estilos del hijo
    //todo elemento distinto va ser usado en el hijo
    //pero cualquier propiedad repetida será sobreescrita
    //por lo que diga el padre por el orden dado abajo
    style={[styles.input, props.style, !props.valid && props.touched ? styles.invalid : null]}
  />
);

const styles = StyleSheet.create({
  input: {
    width: "100%",
    borderWidth: 1,
    borderColor: "#eee",
    padding: 5,
    marginTop: 8,
    marginBottom: 8
  },
  invalid: {
    backgroundColor: '#f9c0c0',
    borderColor: "red"
  }
});

export default defaultInput;
