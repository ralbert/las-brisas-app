import { Navigation } from "react-native-navigation";
import { Provider } from "react-redux";
import AuthScreen from "./src/screens/Auth/Auth";
import FindNewsScreen from "./src/screens/FindNews/FindNews";
import FindTaskScreen from "./src/screens/FindTask/FindTask";
import TaskDetailScreen from "./src/screens/TaskDetail/TaskDetail";
import NewsDetailScreen from "./src/screens/NewsDetail/NewsDetail";
import MainMenuScreen from "./src/screens/MainMenu/MainMenu";
import SideDrawer from "./src/screens/SideDrawer/SideDrawer";
import configureStore from "./src/store/configureStore";

const store = configureStore();
Navigation.registerComponent(
  "las-brisas.AuthScreen",
  () => AuthScreen, store, Provider
);

Navigation.registerComponent(
  "las-brisas.FindNewsScreen",
  () => FindNewsScreen, store, Provider
);

Navigation.registerComponent(
  "las-brisas.FindTaskScreen",
  () => FindTaskScreen, store, Provider
);

Navigation.registerComponent(
  "las-brisas.TaskDetailScreen",
  () => TaskDetailScreen, store, Provider
);
Navigation.registerComponent(
  "las-brisas.NewsDetailScreen",
  () => NewsDetailScreen, store, Provider
);
Navigation.registerComponent(
  "las-brisas.SideDrawer",
  () => SideDrawer, store, Provider
);

Navigation.registerComponent(
  "las-brisas.MainMenuScreen",
  () => MainMenuScreen, store, Provider
);

// Start the App
export default () => Navigation.startSingleScreenApp({
  screen: {
    screen: "las-brisas.AuthScreen",
    title: "Ingresar"
  }
});